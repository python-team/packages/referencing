Source: referencing
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Roland Mas <lolando@debian.org>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-setuptools,
 python3-all,
 pybuild-plugin-pyproject,
 python3-hatchling,
 python3-pytest,
 python3-traitlets,
 jupyter-server,
 python3-rpds-py,
#Testsuite: autopkgtest-pkg-python
Standards-Version: 4.6.2
Homepage: https://github.com/python-jsonschema/referencing
Vcs-Browser: https://salsa.debian.org/python-team/packages/referencing
Vcs-Git: https://salsa.debian.org/python-team/packages/referencing.git

Package: python3-referencing
Architecture: all
Depends:
 ${python3:Depends},
 ${misc:Depends},
Description: Implementation-agnostic implementation of JSON reference resolution
 A way for e.g. JSON Schema tooling to resolve the $ref keywords
 across all drafts without needing to implement support themselves.
 .
 This library is meant for use both by implementers of JSON
 referencing-related tooling – like JSON Schema implementations
 supporting the $ref keyword – as well as by end-users using said
 implementations who wish to then configure sets of resources (like
 schemas) for use at runtime.
 .
 This package installs the library for Python 3.
